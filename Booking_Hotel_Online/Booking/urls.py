from django.urls import path
from . import views

urlpatterns = [
    path('Booking/', views.Booking, name='Booking'),
    path('Bookings/', views.Bookings, name='Bookings'),

]