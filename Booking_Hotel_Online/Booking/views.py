from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

def Bookings(request):
    Layout = loader.get_template('detil.html')
    return HttpResponse(Layout.render())

def Booking(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())
