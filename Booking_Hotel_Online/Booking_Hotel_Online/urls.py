from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('',include('Booking.urls')),
    path('',include('perhotelan.urls')),
    path('admin/', admin.site.urls),
]
